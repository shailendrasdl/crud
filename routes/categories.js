var session = require("express-session");
const flash = require("express-flash");
const express = require("express");
const moment = require("moment");
const UUID = require("uuid-int");

let router = express.Router();
router.use(flash());

var dbConn = require("../config/db");

router.use(
  session({
    secret: "somerandonstuffs",
    resave: false,
    saveUninitialized: false,
    cookie: { expires: 6000000 },
  })
);

router.get("/", function (req, res, next) {
  dbConn.query("SELECT * FROM category ORDER BY id desc", function (err, rows) {
    if (err) {
      req.flash("error", err);
      res.render("categorys", { data: "" });
    } else {
      res.render("categorys", { data: rows, messages: "Welcome" });
    }
  });
});

router.get("/add", function (req, res, next) {
  res.render("categorys/add", {
    messages: "Welcome",
    categoryNmae: "",
    description: "",
    createdAt: "",
    updatedAt: "",
  });
});

router.post("/add", function (req, res, next) {
  let randomNumber = Math.floor(Math.random() * 500 + 1);
  const generator = UUID(randomNumber);
  let publicId = generator.uuid();
  let categoryNmae = req.body.categoryNmae;
  let description = req.body.description;
  let createdAt = moment().format("YYYY-MM-DD");
  let updatedAt = moment().format("YYYY-MM-DD");
  let errors = false;
  if (categoryNmae.length === 0 || description.length === 0) {
    errors = true;
    req.flash("error", "Please enter categoryName and description");
    res.render("categorys/add", {
      publicId: publicId,
      categoryNmae: categoryNmae,
      description: description,
      createdAt: createdAt,
      updatedAt: updatedAt,
    });
  }
  if (!errors) {
    var insetEntity = {
      publicId: publicId,
      categoryNmae: categoryNmae,
      description: description,
      createdAt: createdAt,
      updatedAt: updatedAt,
    };
    dbConn.query(
      "INSERT INTO category SET ?",
      insetEntity,
      function (err, result) {
        if (err) {
          req.flash("error", err);
          res.render("categorys/add", {
            publicId: insetEntity.insetEntity,
            categoryNmae: insetEntity.categoryNmae,
            description: insetEntity.description,
            createdAt: insetEntity.createdAt,
            updatedAt: insetEntity.updatedAt,
          });
        } else {
          req.flash("success", "Category successfully added");
          console.log("Category successfully added");
          res.redirect("/categories");
        }
      }
    );
  }
});

router.get("/edit/(:id)", function (req, res, next) {
  let id = req.params.id;
  dbConn.query(
    "SELECT * FROM category WHERE id = " + id,
    function (err, rows, fields) {
      if (err) throw err;
      if (rows.length <= 0) {
        req.flash("error", "Category not found with id = " + id);
        res.redirect("/categories");
      } else {
        res.render("categorys/edit", {
          messages: "Welcome",
          title: "Edit Category",
          id: rows[0].id,
          publicId: rows[0].publicId,
          categoryNmae: rows[0].categoryNmae,
          description: rows[0].description,
        });
      }
    }
  );
});

router.post("/update/:id", function (req, res, next) {
  let id = req.params.id;
  let categoryNmae = req.body.categoryNmae;
  let description = req.body.description;
  let updatedAt = moment().format("YYYY-MM-DD");
  let errors = false;
  if (categoryNmae.length === 0 || description.length === 0) {
    errors = true;
    req.flash("error", "Please enter name and email and position");
    res.render("categorys/edit", {
      id: req.params.id,
      categoryNmae: categoryNmae,
      description: description,
      updatedAt: updatedAt,
    });
  }
  if (!errors) {
    var updateEntity = {
      categoryNmae: categoryNmae,
      description: description,
      updatedAt: updatedAt,
    };
    dbConn.query(
      "UPDATE category SET ? WHERE id = " + id,
      updateEntity,
      function (err, result) {
        if (err) {
          console.log("Error :", err);
          res.render("categorys/edit", {
            id: req.params.id,
            updatedAt: updatedAt,
            description: updateEntity.description,
            categoryNmae: updateEntity.categoryNmae,
          });
        } else {
          req.flash("success", "Category Updated Successfully.. ");
          console.log("Category successfully updated");
          res.redirect("/categories");
        }
      }
    );
  }
});

router.get("/delete/(:id)", function (req, res, next) {
  dbConn.query(
    "DELETE FROM category WHERE id = " + req.params.id,
    function (err, result) {
      if (err) {
        req.flash("error", err);
        res.redirect("/categories");
      } else {
        console.log("Category successfully deleted! ID :", req.params.id);
        res.redirect("/categories");
      }
    }
  );
});

module.exports = router;
