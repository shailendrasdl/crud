const session = require("express-session");
const flash = require("express-flash");
const express = require("express");
const moment = require("moment");
const UUID = require("uuid-int");
const router = express.Router();

router.use(flash());

var dbConn = require("../config/db");

router.use(
  session({
    secret: "somerandonstuffs",
    resave: false,
    saveUninitialized: false,
    cookie: { expires: 6000000 },
  })
);

router.get("/", function (req, res, next) {
  dbConn.query(
    "SELECT * FROM product ORDER BY id desc",
    function (err, result) {
      if (err) {
        req.flash("error", err);
        res.render("product/products", { data: "" });
      } else {
        res.render("product/products", { data: result, messages: "Wel" });
      }
    }
  );
});

router.get("/add", function (req, res, next) {
  dbConn.query(
    "SELECT * FROM category ORDER BY id desc",
    function (err, result) {
      if (err) {
        req.flash("error", err);
        res.render("categorys", { data: "" });
      } else {
        res.render("product/addProduct", {
          categories: result,
          msg: "",
          messages: "",
          productName: "",
          price: "",
          quantity: "",
          createdAt: "",
          updatedAt: "",
        });
      }
    }
  );
});

router.post("/add", function (req, res, next) {
  const randomNumber = Math.floor(Math.random() * 500 + 1);
  const generator = UUID(randomNumber);
  const publicId = generator.uuid();
  const categoryNmae = req.body.category;
  const productName = req.body.productName;
  const price = req.body.price;
  const quantity = req.body.quantity;
  let createdAt = moment().format("YYYY-MM-DD");
  let errors = false;
  if (categoryNmae.length === 0 || productName.length === 0) {
    errors = true;
    console.log("Please enter categoryNmae and productName & price");
    res.render("product/addProduct", {
      publicId: publicId,
      categoryNmae: categoryNmae,
      productName: productName,
      price: price,
      quantity: quantity,
      createdAt: createdAt,
    });
  }
  if (!errors) {
    var insetEntity = {
      price: price,
      quantity: quantity,
      publicId: publicId,
      categoryNmae: categoryNmae,
      productName: productName,
      createdAt: createdAt,
    };
    dbConn.query(
      "INSERT INTO product SET ?",
      insetEntity,
      function (err, result) {
        if (err) {
          console.log("err:", err);
          req.flash("error", "error");
          res.render("product/addProduct", {
            price: insetEntity.price,
            publicId: insetEntity.publicId,
            quantity: insetEntity.quantity,
            productName: insetEntity.productName,
            categoryNmae: insetEntity.categoryNmae,
            createdAt: insetEntity.createdAt,
          });
        } else {
          req.flash("success", "Product successfully added");
          console.log("Product successfully added");
          res.redirect("/products");
        }
      }
    );
  }
});

router.get("/edit/(:id)", function (req, res, next) {
  dbConn.query(
    "SELECT * FROM product WHERE id = " + req.params.id,
    function (err, result, fields) {
      if (err) throw err;
      if (result.length <= 0) {
        req.flash("error", "Product not found with id = " + req.params.id);
        res.redirect("/products");
      } else {
        res.render("product/editProduct", {
          messages: "Welcome",
          title: "Edit Product",
          id: result[0].id,
          publicId: result[0].publicId,
          categoryNmae: result[0].categoryNmae,
          productName: result[0].productName,
          price: result[0].price,
          quantity: result[0].quantity,
          description: result[0].description,
        });
      }
    }
  );
});

router.post("/update/:id", function (req, res, next) {
  let id = req.params.id;
  var categoryNmae = req.body.categoryNmae;
  let productName = req.body.productName;
  var price = req.body.price;
  var quantity = req.body.quantity;
  let updatedAt = moment().format("YYYY-MM-DD");
  let errors = false;
  if (productName == null) {
    errors = true;
    res.render("product/editProduct", {
      id: req.params.id,
      categoryNmae: categoryNmae,
      productName: productName,
      price: price,
      quantity: quantity,
      description: description,
      updatedAt: updatedAt,
    });
  }
  if (!errors) {
    var updateEntity = {
      categoryNmae: categoryNmae,
      productName: productName,
      price: price,
      quantity: quantity,
      updatedAt: updatedAt,
    };
    dbConn.query(
      "UPDATE product SET ? WHERE id = " + id,
      updateEntity,
      function (err, result) {
        if (err) {
          console.log("error", err);
          res.render("product/editProduct", {
            id: req.params.id,
            price: updateEntity.price,
            categoryNmae: updateEntity.categoryNmae,
            productName: updateEntity.productName,
            quantity: updateEntity.quantity,
            updatedAt: updateEntity.updatedAt,
          });
        } else {
          console.log("Product successfully updated");
          res.redirect("/products");
        }
      }
    );
  }
});

router.get("/delete/(:id)", function (req, res, next) {
  dbConn.query(
    "DELETE FROM product WHERE id = " + req.params.id,
    function (err, result) {
      if (err) {
        req.flash("error", err);
        res.redirect("/products");
      } else {
        console.log("Product successfully deleted! ID :", req.params.id);
        res.redirect("/products");
      }
    }
  );
});

module.exports = router;
