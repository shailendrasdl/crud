const bodyParser = require("body-parser");
const flash = require("express-flash");
var session = require("express-session");
const express = require("express");
const path = require("path");

const ejs = require("ejs");

const app = express();
const dbConn = require("./config/db");
const categories = require("./routes/categories");
const products = require("./routes/products");

app.use(
  session({
    secret: "somerandonstuffs",
    resave: false,
    saveUninitialized: false,
    cookie: { expires: 6000000 },
  })
);

app.use(flash());
app.use(bodyParser.json());
app.set("view engine", "ejs");
app.set("views", path.join(__dirname, "views"));
app.use(bodyParser.urlencoded({ extended: false }));

app.get("/", (req, res) => {
  res.send(
    "CRUD Operation using NodeJS / ExpressJS / MySQL  & EJS template-engines !" +
      "http://localhost:9000/categories"
  );
});
app.use("/categories", categories);
app.use("/products", products);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

app.listen(9000, () => {
  console.log("Server is running at port 3000");
});
